-- Add users
CALL `woofing`.add_user(`jack@email.com`,`Jack`,`Jean`);
CALL `woofing`.add_user(`jean@email.com`,`Jean`,`Jack`);
CALL `woofing`.add_user(`john@email.com`,`John`,`Jean`);

-- Add events
CALL `woofing`.add_event(`Build 01`, `Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups..`, `2021-12-18`, `2021-12-28`, 6);
CALL `woofing`.add_event(`Build 02`, `Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups..`, `2021-05-18`, `2021-05-28`, 12);
CALL `woofing`.add_event(`Build 03`, `Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups..`, `2021-07-18`, `2021-07-28`, 3);

-- Assign adress to events
CALL assign_address_to_event(5, "avenue du Champ", "Bruxelles", "Belgique", "1000");
CALL assign_address_to_event(245, "avenue du Blé", "Isère", "France", "38000");
CALL assign_address_to_event(34, "avenue du Canton", "Lyon", "France", "69005");
