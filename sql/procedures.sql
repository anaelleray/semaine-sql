-- Register

DELIMITER ;
CREATE DEFINER=`vegalyrae`@`localhost` PROCEDURE `add_user`(
    IN i_email VARCHAR(255), 
    IN i_first_name VARCHAR(255), 
    IN i_last_name VARCHAR(255), 
    IN i_password VARCHAR(512), 

)
BEGIN
    -- Password encryption
	SET i_password = SHA2(i_password,512);
	INSERT INTO `user` 
	(
		email,
		first_name,
		last_name,
		password,
	) 
	VALUES 
	(
		i_email,
		i_first_name,
		i_last_name,
		i_password,
	);
END;

-- Event
DELIMITER ;
CREATE DEFINER=`vegalyrae`@`localhost` PROCEDURE `add_event`(
    IN i_title VARCHAR(255),
    IN i_description VARCHAR(512), 
    IN i_start_date DATE, 
    IN i_end_date DATE, 
    IN i_capacity TINYINT(4)
)
BEGIN
	INSERT INTO `event` 
    (
        title,
        description,
        start_date,
        end_date,
        capacity
    ) 
    VALUES 
    (
        i_title,
        i_description,
        i_start_date,
        i_end_date,
        i_capacity
        
	);
END ;

-- Address

DELIMITER ;
CREATE DEFINER=`vegalyrae`@`localhost` PROCEDURE `add_address`(
	IN i_event_id INT,
	IN i_street_number VARCHAR(45),
	IN i_street_name VARCHAR(155),
	IN i_city VARCHAR(155),
    IN i_country VARCHAR(155)
	IN i_zip_code VARCHAR(45),
	
)
BEGIN
	DECLARE var_address_id INT;
    
	INSERT INTO `address`
	(
        street_number,
        street_name,
        city,
        country,
        zip_code,
	) 
    VALUES 
    (
        i_street_number,
        i_street_name,
        i_city,
        i_country,
        i_zip_code,
    );
    -- Get last ID used
    SELECT LAST_INSERT_ID() INTO var_address_id  FROM `address` LIMIT 1;
    UPDATE `event` SET `address_id` = var_address_id WHERE `id` = i_event_id;
    
END ;


